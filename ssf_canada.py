#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yfinance as yf
import requests
from datetime import datetime 
from bs4 import BeautifulSoup
import re


#titles = ["DFN.TO", "DF.TO", "FTN.TO", "FFN.TO", "BK.TO", "SBC.TO", "LBS.TO", "GDV.TO", "DGS.TO", "LCS.TO", "PWI.TO", "ENS.TO", "RS.TO"]
titles = ["LCS.TO"]


# Dictionnaire contenant les informations supplémentaires pour chaque titre
fund_data = {
    'DFN.TO': 'Quadravest',
    'DF.TO': 'Quadravest',
    'FTN.TO': 'Quadravest',
    'FFN.TO': 'Quadravest',
    'BK.TO': 'Quadravest',
    'SBC.TO': 'Brompton',
    'LBS.TO': 'Brompton',
    'GDV.TO': 'Brompton',
    'DGS.TO': 'Brompton',
    'LCS.TO': 'Brompton',
    'PWI.TO': 'Brompton',
    'ENS.TO': 'Middlefield',
    'RS.TO': 'Middlefield'
}

fund_data_url = {
    'DFN.TO': 'https://www.quadravest.com/dfn-fund-features',
    'DF.TO': 'https://www.quadravest.com/df-fund-features',
    'FTN.TO': 'https://www.quadravest.com/ftn-fund-features',
    'FFN.TO': 'https://www.quadravest.com/ffn-fund-features',
    'BK.TO': 'https://www.quadravest.com/bk-fund-features',
    'SBC.TO': 'https://www.bromptongroup.com/product/brompton-split-banc-corp/',
    'LBS.TO': 'https://www.bromptongroup.com/product/life-banc-split-corp/',
    'GDV.TO': 'https://www.bromptongroup.com/product/global-dividend-growth-split-corp/',
    'DGS.TO': 'https://www.bromptongroup.com/product/dividend-growth-split-corp/',
    'LCS.TO': 'https://www.bromptongroup.com/product/brompton-lifeco-split-corp/',
    'PWI.TO': 'https://www.bromptongroup.com/product/sustainable-power-infrastructure-split-corp/',
    'ENS.TO': 'https://middlefield.com/funds/split-share-funds/e-split-corp/',
    'RS.TO': 'https://middlefield.com/funds/split-share-funds/real-estate-split-corp/'
}

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}

# Dictionnaire pour stocker les données traitées pour chaque action
stock_data = {}


def get_additional_data(title):
    # Récupération de l'URL de la page web
    url =fund_data_url[title]
    print(url)    

    # Extraction du code HTML de la page web
    r = requests.get(url, headers=headers)
    html_content = r.text

    # Création de l'objet BeautifulSoup pour parser le code HTML
    soup = BeautifulSoup(html_content, "html.parser")
    
    # Quadravest    
    if fund_data[title] == "Quadravest":

        # Récupération de la date de NAV
        nav_date_line = soup.find("p", class_="font_8", style="line-height:2.5em; font-size:13px;").text
        nav_date = nav_date_line.split("\xa0")[0]
        nav_date = nav_date.strip()
        nav_date = datetime.strptime(nav_date, "%B %d, %Y")

        # Récupération de UnitNAV
        lines = nav_date_line.split("\n")
        unit_nav = float(lines[1].split("$")[1])
        
        # Extraction de la valeur pour ClassANavPrice
        class_a_nav_price = unit_nav - 10

    # Brompton
    elif fund_data[title] == "Brompton":

        # Récupération de la ligne contenant `<span class="basic_detail_label">NAV</span>`
        nav_line = soup.find("span", class_="basic_detail_label", text="NAV")

        # Date du NAV
        next_line = nav_line.find_next_sibling()
        # On retire les parenthèse
        nav_date = re.sub(r'[\(\)]', '', next_line.text)
        nav_date =   datetime.strptime(nav_date.strip(), "%b %d, %Y")
        # Valeur du NAV
        class_a_nav_price = nav_line.find_next("span", class_="basic_detail_value").text
        class_a_nav_price  = float(class_a_nav_price .strip("$"))
        
        # Unit NAV
        nav_line = soup.find("span", class_="basic_detail_label", text="Unit NAV")
        unit_nav = nav_line.find_next("span", class_="basic_detail_value").text
        unit_nav  = float(unit_nav.strip("$"))

    # Mise à jour des données du dictionnaire
    stock_data[title]["NavDate"] = nav_date
    stock_data[title]["ClassANavPrice"] = class_a_nav_price
    stock_data[title]["UnitNAV"] = unit_nav
    #stock_data[title]["marketPriceDate"] = market_price_date
    #stock_data["premiumDiscount"] = stock_data['regularMarketPrice'] - class_a_nav_price

    return stock_data[title]

# Dictionary to store stock data
stock_data = {}

# Get data for each stock
for title in titles:
    # Get data from Yahoo Finance
    ticker = yf.Ticker(title)
    info = ticker.info
    
    stock_data[title] = {}
    stock_data.setdefault(title, {})["Price"]= info["regularMarketPrice"]
    stock_data[title]["Dividend"] = info["dividendRate"]
    stock_data[title]["regularMarketPreviousClose"] = info['regularMarketPreviousClose']
# Récupération de la date du dernier cours de l'action
    stock_data[title]["datePreviousClose"]  = info["regularMarketPreviousClose"]["fmt"]

 # Check that variables are not 'None' before multiplying
    if stock_data[title]["Price"] is not None and info['dividendYield'] is not None:
        stock_data[title]['annualized_return'] =  info['dividendYield']*100 
    else:
        stock_data[title]['annualized_return'] =  'N/A '
 
    # Get additional data from funds website
    if any(fund_data[title] in s for s in fund_data.values()):
        additional_data = get_additional_data(title)
        stock_data[title].update(additional_data)

# Print stock data
for title, data in stock_data.items():
    print(title)
    for key, value in data.items():
        print(f"  {key}: {value}")


## Print data for each stock
#for title, data in stock_data.items():
    #print(f"Data for stock {title}:")
    #print(f"Class A Market Price: {data['regularMarketPrice']:>10.2f}$")
    #print(f"Date du dernier cours de l'action:{data['regularMarketPreviousClose']:>10.2f}$")
    #print(f"Last dividend: {data['dividendRate']:>10.2f}")
    
    ## Calculate annualized return for dividends
    #current_price = data['regularMarketPrice']
    #dividend_yield = data['dividendYield']

    ## Check that variables are not 'None' before multiplying
    #if current_price is not None and dividend_yield is not None:
        #annualized_return = dividend_yield*100
        #print(f"Annualized return for dividends: {annualized_return:10.2f}%")
    #else:
        #print("Unable to calculate annualized return for dividends")
    
    ## Print additional data
    #print(f"NAV date: {data['NavDate']}")
    #print(f"Class A NAV price: {data['ClassANavPrice']:>10.2f}$")
    #print(f"Unit NAV: {data['UnitNAV']:>10.2f}$")
    ##print(f"Market price date: {data['marketPriceDate']}")
    ##print(f"Class A market price: {data['classAMarketPrice']:>10.2f}$")
    ##print(f"Premium discount: {data['premiumDiscount']:>10.2f}%")
    #print(f"Fund: {fund_data[title]}")
    #print()



